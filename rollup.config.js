import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import styles from "rollup-plugin-styles";
import html from '@rollup/plugin-html';
import {indexTemplate} from './build-util/index-template'
import {generateDist} from './build-util/generate-dist'
import copy from 'rollup-plugin-copy'

const production = !process.env.ROLLUP_WATCH;
const build_dir = "public";
const prod_dir = "dist";

function serve() {
	let server;

	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;
			server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
				stdio: ['ignore', 'inherit', 'inherit'],
				shell: true
			});

			process.on('SIGTERM', toExit);
			process.on('exit', toExit);
		}
	};
}

export default {
	input: 'src/main.ts',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		dir: 'public',
		entryFileNames: `build/[name]${production ? '.[hash]' : ''}.js`,
		chunkFileNames: `build/[name].chunk${production ? '.[hash]' : ''}.[ext]`, // Have not tested chunks
		assetFileNames: `build/[name]${production ? '.[hash]' : ''}.[ext]`,
	},
	plugins: [
		copy({
			targets: [
				{ src: 'node_modules/@fontsource/material-icons/*', dest: 'public/assets/material-icons' },
				{ src: 'node_modules/@fontsource/roboto/*', dest: 'public/assets/roboto' },
				{ src: 'node_modules/@fontsource/roboto-mono/*', dest: 'public/assets/roboto-mono' },
			],
			overwrite: false,
			copyOnce: true
		}),

		svelte({
			preprocess: sveltePreprocess({ sourceMap: !production }),
			compilerOptions: {
				// enable run-time checks when not in production
				dev: !production
			},
			onwarn: (warning, handler) => {
				// don't warn 'A11y: A form label must be associated with a control'
				if (warning.filename.startsWith('node_modules\\@smui') && warning.code === 'a11y-label-has-associated-control') return;
				// let Rollup handle all other warnings normally
				handler(warning);
			}
		}),

		// rollup-plugin-styles transpiles to CSS and extracts the final css
		styles({
			mode: "extract",
			minimize: production,
			sass: {
				includePaths: [
					'./src/theme',
					'./node_modules'
				]
			}
		}),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
		typescript({
			sourceMap: true,
			inlineSources: !production
		}),

		// In dev mode, call `npm run start` once
		// the bundle has been generated
		!production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload('public'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser({ output: { comments: false } }),

		// For production builds index.html needs to be rewritten with filenames containing th hashes
		// note: this only covers the bundle files (global.css is fixed later)
		production && html({
			fileName: 'build/index.html', // Write to build folder so we dont overwrite the dev one
			title: 'Svelte app',
			publicPath: '/',
			attributes: { html: {lang: 'en'} },
			meta: [{ charset: 'utf-8'}, {name: 'viewport', content:'width=device-width,initial-scale=1'}],
			include: "**/*.html",
			template: indexTemplate,
		}),

		// Production builds will be moved to the dist folder and a hash added to global.css
		production && generateDist(build_dir, prod_dir)
	],
	watch: {
		clearScreen: false
	}
};
