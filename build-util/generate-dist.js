// generate-dist handles the creation of the final distribution. This copies files from public, moves a few things
// around and adds a hash to global.css
// Code based on https://github.com/metonym/hash-static
import fs from 'fs';
import fse from 'fs-extra';
import path from 'path';
import posthtml from 'posthtml';
import hash from 'posthtml-hash'
import urls from 'posthtml-urls';

// generateDist creates the dist folder and renames files to include hashes in order to avoid cache issues
export function generateDist(build_dir = 'public', dist_dir = 'dist') {
    return {
        name: "generate-dist",
        buildStart() {
            // It is important that the build directory is emptied before the build starts because otherwise
            // we may have some duplicate bundles etc
            fs.rmdirSync(`${build_dir}/build`, {recursive: true});
        },
        writeBundle() {
            fs.rmdirSync(`${dist_dir}`, {recursive: true});
            fse.copySync(build_dir, dist_dir); // Copy everything in build_dir to dist_dir

            // We will be processing index.html so need the new ond (from /build)
            fs.renameSync(`${dist_dir}/build/index.html`, `${dist_dir}/index.html`);

            // Next we want to add a hash to the global.css file (and update index.html to reflect this)
            posthtml([
                urls({
                    eachURL: (url, attr, element) => {
                        if (url.startsWith('/build/')) {
                            const fn = url.substring(7)
                            console.log(url, fn);
                            fs.renameSync(`${dist_dir}/build/${fn}`, `${dist_dir}/${fn}`);
                            return `/${fn}`
                        }
                        return url; // no change
                    }
                }),
                // Next we rename global.css to global.[hash].css (makes it simple to use posthtml-hash)
                (tree) => {
                    const exp = '/global.css'; // We are only interested in the one file
                    return tree.match([{attrs: {href: exp}}, {attrs: {src: exp}}, {attrs: {content: exp}}],
                        (node) => {
                            const {href, src, content} = node.attrs;
                            const fileName = href || src || content;
                            let newName = '/global.[hash].css';
                            let srcPath = path.join(dist_dir, fileName);
                            let dstPath = path.join(dist_dir, newName);
                            if (fs.existsSync(srcPath) && !fs.existsSync(dstPath)) {
                                fs.renameSync(srcPath, dstPath);
                            }
                            if (href) {
                                node.attrs.href = newName;
                            } else if (src) {
                                node.attrs.src = newName;
                            } else if (content) {
                                node.attrs.content = newName;
                            }
                            return node;
                        });
                },
                hash({path: dist_dir, hashLength: 8})
            ])
                .process(fs.readFileSync(`${dist_dir}/index.html`))
                .then((result) => {
                        fs.writeFileSync(`${dist_dir}/index.html`, result.html)
                    }
                );

            // The only thing left in build should be the map file so lets move everything and remove the folder
            fs.readdirSync(`${dist_dir}/build`).forEach(file => {
                fs.renameSync(`${dist_dir}/build/${file}`, `${dist_dir}/${file}`);
            });
            fs.rmdirSync(`${dist_dir}/build`);
        }
    }
}
