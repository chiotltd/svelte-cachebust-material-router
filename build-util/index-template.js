/* indexTemplate.js

Used by @rollup/plugin-html to generate the template. This is a modified version of the defaultTemplate provided
within @rollup/plugin-html (the aim is to match the standard index.html as closely as possible).
*/

// makeHtmlAttributes is copied from @rollup/plugin-html with slight modifications (wanted the quotes used to match the
// original template - not really required but minimises the changes from the base template)
const makeHtmlAttributes = (attributes, quote=`'`) => {
    if (!attributes) {
        return '';
    }

    const keys = Object.keys(attributes);
    // eslint-disable-next-line no-param-reassign
    return keys.reduce((result, key) => (result += ` ${key}=${quote}${attributes[key]}${quote}`), '');
};

// indexTemplate generates the template (copied from default in @rollup/plugin-html and modified to fit our needs)
export const indexTemplate = async ({ attributes, files, meta, publicPath, title }) => {
    const scripts = (files.js || [])
        .map(({ fileName }) => {
            const attrs = makeHtmlAttributes(attributes.script);
            return `<script defer src='${publicPath}${fileName}'${attrs}></script>`;
        })
        .join('\n');

    const links = (files.css || [])
        .map(({ fileName }) => {
            const attrs = makeHtmlAttributes(attributes.link);
            return `<link rel='stylesheet' href='${publicPath}${fileName}'${attrs}>`;
        })
        .join('\n');

    const metas = meta
        .map((input) => {
            const attrs = makeHtmlAttributes(input);
            return `<meta${attrs}>`;
        })
        .join('\n');

    return `
<!DOCTYPE html>
<html${makeHtmlAttributes(attributes.html, '"')}>
  <head>
    ${metas}
   
    <title>${title}</title>
   
    <link rel='icon' type='image/png' href='/favicon.png'>
    <link rel='stylesheet' href='/global.css'>

    ${links}
    
    ${scripts}
    
    <link rel="stylesheet" href="/assets/roboto/index.css">
    <link rel="stylesheet" href="/assets/roboto-mono/index.css">        
</head>

<body>
</body>
</html>`;
};