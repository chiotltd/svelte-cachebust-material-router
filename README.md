# Svelte - Template with extras

This folder contains a template based upon the standard [sveltejs/template](https://github.com/sveltejs/template) (as at 2021-01-25; commit 47ed448d2a9905dd6876122f85fbcee825fa8628) with the following additions:

* Cache busting in the production build (filenames have a hash added)
* [svelte-material-ui](https://github.com/hperrin/svelte-material-ui) - library of Svelte 3 Material UI components,
* [typescript](https://www.typescriptlang.org/) - typesafe coding.
* [tinro](https://github.com/AlexxNB/tinro) - A simple router.

This readme steps through the process followed to create the template (and will simplify the process of updating it when new versions of the base template are released). Please see the readme in the source template if you are after general info.

If you wish to use this template you can grab it as-is from the repository or follow the below instructions to create your own version. Note that changes to the base template may mean that the below instructions do not work exactly as written!

*Note: This was created as I investigated svelte for the first time so there are probably better alternatives!*

## Install Base template

This is covered in the [svelte blog](https://svelte.dev/blog/the-easiest-way-to-get-started) but the basic commands are:

```bash
npx degit sveltejs/template my-svelte-project
cd my-svelte-project
npm install
npm run dev
```
This will start the template app; you can view it on http://localhost:5000.

`npm run build` will to create a 'production-ready' version of the app (excludes runtime checks/debugging code and uses [terser](https://github.com/terser/terser) to make it smaller).

*Note: I created this repository and then copied the template into it with `npx degit sveltejs/template .\svelte-cachebust-material-router --force`*

*Note2: `.gitignore` was updated to avoid committing unnecessary files.*

## Install `rollup-plugin-styles`

The default template uses `rollup-plugin-css-only` to combine and output the projects css files. This works OK in a very basic application but unfortunately it does not support the rollup [`assetFileNames`](https://rollupjs.org/guide/en/#outputassetfilenames) option (this is why the default template passes the name to the plugin `css({ output: 'bundle.css' })`).  Using the `assetFileNames` option (with plugins that support it) enables us to control the names of the files that rollup will output in one place and really simplifies things like cache busting.

We are going to need the ability to  transpile scss files into css when deploying svelte-material-ui and the plugin we will use for that ([`rollup-plugin-styles`](https://anidetrix.github.io/rollup-plugin-styles/index.html)) also combines/emits css files so it makes sense to install than now. Firstly install the plugin:

````bash
npm install -D rollup-plugin-styles
````

Next we need to edit `/rollup.config.js` so that this will be used. Firstly replace `import css from 'rollup-plugin-css-only';` with `import styles from "rollup-plugin-styles";`. Next replace

```javascript
// we'll extract any component CSS out into
// a separate file - better for performance
css({ output: 'bundle.css' }),
​```
```

with:

```
// rollup-plugin-styles transpiles to CSS and extracts the final css 
styles({
    mode: "extract",
    minimize: production,
}),
```

We also need to update the `output` section so it uses the new directives to define where the output should be written. Replace:

```
output: {
    sourcemap: true,
    format: 'iife',
    name: 'app',
    file: 'public/build/bundle.js'
},
```

with

```
output: {
    sourcemap: true,
    format: 'iife',
    name: 'app',
    dir: 'public',
    entryFileNames: `build/[name].js`,
    chunkFileNames: `build/[name].js`,
    assetFileNames: `build/[name].[ext]`,
},
```

Finally we need to update `public/index.html` so that it uses the new names for the bundle files; replace:

```html
<link rel='stylesheet' href='/build/bundle.css'>
<script defer src='/build/bundle.js'></script>
```

with:

```html
<link rel='stylesheet' href='/build/main.css'>
<script defer src='/build/main.js'></script>
```

*Note: [`rollup-plugin-styles`](https://anidetrix.github.io/rollup-plugin-styles/index.html) was chosen over the more common [`rollup-plugin-postcss`](https://github.com/egoist/rollup-plugin-postcss) because of its support for [`assetFileNames`](https://rollupjs.org/guide/en/#outputassetfilenames)*

## Cache busting for production build

*Note: This step can be skipped if it's not needed for your project.  If you are using a CDN (or webserver configured to add hashes to urls) then there may be no need to implement this yourself.*

`npm run build` generates files (in `/public/build`) that you could deploy to production but there may be issues when applying upgrades. This is because the app is in ```build/bundle.css``` and ```build/bundle.js``` and the browser may cache these (this depends upon webserver configuration but ideally you want these files to be cached except when an update has been deployed). A fairly standard way to work around this is to add a hash to the filenames of the application components and update the `index.html` to load the files with the current hashes (this means that when the file changes its name also changes meaning the browser will request it from the server). [This article](https://medium.com/@codebyamir/a-web-developers-guide-to-browser-caching-cc41f3b73e7c) provides a good overview.

*Note*: There are a few open issues ([sveltejs #39](https://github.com/sveltejs/template/issues/39) [sveltejs #61](https://github.com/sveltejs/rollup-plugin-svelte/issues/61), [rollup-plugin-css-only #25](https://github.com/thgh/rollup-plugin-css-only/issues/25)) and the rapid rate of change with Svelte/Rollup means documentation/tutorials are often out of date. While there were some solutions available ([svelte-rollup-template](https://github.com/metonym/svelte-rollup-template),
[obs-web](https://github.com/Niek/obs-web/blob/master/rollup.config.js)) these did not make use of recently introduced functionality (e.g. `assetFileNames` in rollup) and I encountered some issues when testing them. As a result I have created my own solution (drawing on a range of resources) in an attempt to come up with something that works for me.

My aims are:

* Make the process understandable (avoid adding too many dependencies that make it difficult to follow how this works).
* Have as little impact on the development process as possible (ideally `npm run dev` should work exactly as it does in the base template).
* Efficienct use of cache - files should be cached unless a change has been made (eliminates options that add the build date to the filename).  
* Keep changes to `rollup.config.js` to a minimum to make it easier to update to new versions of the template.
* Use `output.entryFileNames` and `output.chunkFileNames` options as suggested by [Rich Harris](https://twitter.com/rich_harris/status/1079991930623283200)
* Deploy production builds to a separate folder (`/dist`) to simplify deployment and ensure development builds do not end up in production!

Hopefully the process can be simplified in the future.

### Copy in new files

The `/build-util` folder needs to be created and a few utility functions copied in (grab the folder from this repository). These javascript files will be used in `rollup.config.js` (putting these in a separate file minimises the changes needed there). When rebuilding from scratch it is probably simplest to copy these files from this repo. The files are:

* index-template.js - Code to generate an `index.html` that references the bundled files using names that include the hash.
* generate-dist.js - Code to copy `/public/` to `/dist/` and make the changes needed to get everything running.

The files all contain some comments so hopefully it is not too difficult to see what they are doing (and fix any issues!).

### Install required modules

Run:
```bash
npm install -D @rollup/plugin-html posthtml posthtml-hash posthtml-urls fs-extra
```

These modules are:

* `@rollup/plugin-html` - A Rollup plugin which creates HTML files to serve Rollup bundles ([github](https://github.com/rollup/plugins/tree/master/packages/html#readme)).
* `posthtml` - a tool for transforming HTML/XML with JS plugins ([github](https://github.com/posthtml/posthtml)).
* `posthtml-hash` - plugin for the above that adds hashes to filenames (by default it replaces the `hash` within a filename something like `bundle.[hash].js`) 
* `posthtml-urls` - plugin for posthtml that simplifies finding/changing urls within a html file.

### Update `rollup.config.js`

Because most of the work is done in `build-util` the changes needed in `rollup.config.js` are relatively simple:

#### includes

Under the existing includes add:

```javascript
import html from '@rollup/plugin-html';
import {indexTemplate} from './build-util/index-template'
import {generateDist} from './build-util/generate-dist'
```

#### Add constants

Add the following after the imports (after the existing `const production`):

```javascript
const build_dir = "public";
const prod_dir = "dist";
```

### Reconfigure rollup outputs

We need to update the output filenames so that these will include the hash (in production only). Update these (under `default.output`) to be:

```javascript
entryFileNames: `build/[name]${production ? '.[hash]' : ''}.js`,
chunkFileNames: `build/[name].chunk${production ? '.[hash]' : ''}.js`,
assetFileNames: `build/[name]${production ? '.[hash]' : ''}.[ext]`,
```

This change tells rollup to add hashes to the files it generates for production builds.

#### Generate `index.html` and `dist` folder

Under ```production && terser()``` (add a `,` to the end of this line) add:

```javascript
// For production builds index.html needs to be rewritten with filenames containing th hashes
// note: this only covers the bundle files (global.css is fixed later)
production && html({
    fileName: 'build/index.html', // Write to build folder so we dont overwrite the dev one
    title: 'Svelte app',
    publicPath: '/',
    attributes: { html: {lang: 'en'} },
    meta: [{ charset: 'utf-8'}, {name: 'viewport', content:'width=device-width,initial-scale=1'}],
    include: "**/*.html",
    template: indexTemplate,
}),

// Production builds will be moved to the dist folder and a hash added to global.css
production && generateDist(build_dir, prod_dir)
```

### Test everything still works

Run `npm run dev` and verify that everything still works.

Run `npm run build` and verify that a `dist` folder is created and populated. 

To test that the production files can be served OK lets add a new run action in `package.json`. Under `"start": "sirv public"` add `"serveprod": "sirv dist"` then run `npm run serveprod`; you should see the hello world page.

## Install svelte-material-ui and supporting components

### Fonts

The material design styles use a few fonts which we need to install. Prior to doing this lets add a small test (following to the end of `<main>` in `src/App.svelte`) so it’s obvious when the fonts are correctly installed:

```html
<p style="font-family: 'Roboto', monospace">Roboto</p>
<p style="font-family: 'Roboto Mono', monospace">Roboto Mono</p>
<i class="material-icons">face</i>
```

If you test the app you will note that this does not display correctly (you will see “face” rather than a face icon). This provides a way to check if your setup is working.

#### Google Hosted

Add the following to the bottom of the `<head>` section in `public/index.html` (and the template at the bottom of `build-util/index-template.js` if you are using cache busting as per the previous section) :

```css
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Mono">
```

Start the app and check that the example (added above) looks right.

#### Self Hosted

*If you have added the google hosted fonts then remove them before proceeding*

If you don't want to rely upon google to host the fonts then you can self-host them. There are a range of npm packages that can be used. I chose to use the @fontsource packages (because they had packaged all the fonts I was interested in).

Now install the modules and a rollup plugin we will use to copy them where they are needed:

```
npm install -D  @fontsource/material-icons @fontsource/roboto @fontsource/roboto-mono rollup-plugin-copy
```

There are ways to get rollup to process the fonts; we will just add some copy commands to `rollup.config.js` to automate this (simplifying first use of the template and, potentially, meaning that updated versions of the fonts will be deployed with a new build). Note that an alternative is to just copy them into the public folder (as suggested by [Rich](https://stackoverflow.com/a/52204593/11810946))

Open `rollup.config.js` and add the following at the bottom of the existing imports:
```javascript
import copy from 'rollup-plugin-copy'
```

Now add the following at the top of the `plugins: [`:

```javascript
copy({
            targets: [
                { src: 'node_modules/@fontsource/material-icons/*', dest: 'public/assets/material-icons' },
                { src: 'node_modules/@fontsource/roboto/*', dest: 'public/assets/roboto' },
                { src: 'node_modules/@fontsource/roboto-mono/*', dest: 'public/assets/roboto-mono' },
            ],
    		overwrite: false,
            copyOnce: true
        }),
```

*Note: The inclusion of `overwrite:false` means that updates to the fonts will not be copied. This option is used because it speeds up builds considerably*

When you run `npm run dev` this will copy the npm modules into `public/assets`/ As these files come from `note_modules` there is no need to commit them so add  `public/assets` to `.gitignore` (and be careful not to add anything in there that does need to be kept!).

Add the following to `public/index.html` (and updating `build-util/index-template.js` if you are using cache busting as per the previous section) :

```css
<link rel="stylesheet" href="/assets/roboto/index.css">
<link rel="stylesheet" href="/assets/roboto-mono/index.css">
```

The icon fonts are not quite as easy to install (I checked a number of distributions and all have some issues). The simplest approach seems to be to add the following at the top of `public/global.css`:

```css
@font-face {
font-family: 'Material Icons';
font-style: normal;
font-weight: 400;
src: url('/assets/material-icons/files/material-icons-outlined-400-normal.woff2') format('woff2'), url('/assets/material-icons/files/material-icons-outlined-400-normal.woff') format('woff');
}

.material-icons {
font-family: 'Material Icons';
font-weight: normal;
font-style: normal;
font-size: 24px;
line-height: 1;
letter-spacing: normal;
text-transform: none;
display: inline-block;
white-space: nowrap;
word-wrap: normal;
direction: ltr;
-moz-font-feature-settings: 'liga';
-moz-osx-font-smoothing: grayscale;
}
```

Run `npm run dev` and confirm that the fonts are correctly loaded.

### svelte-material-ui

A number of steps are required to setup the material UI package and these are not well documented. [This issue](https://github.com/hperrin/svelte-material-ui/issues/36#issuecomment-612049847) contains a fairly simple series of steps and I will follow a similar process.

#### Test Page

Prior to installing the modules etc lets add some example code (from [the readme](https://github.com/hperrin/svelte-material-ui)) to `App.svelte` so that we can see if things are working. Firstly add the following to the bottom of the existing `script` section (top of file)

```javascript
import Button from '@smui/button';
import Fab from '@smui/fab';
import Textfield from '@smui/textfield';
import HelperText from '@smui/textfield/helper-text';
import {Label, Icon} from '@smui/common';

let superText = '';
```

then the following at the bottom of `main`:

```javascript
<Button on:click={() => alert('Clicked!')}>Just a Button</Button>
<Button variant="raised"><Label>Raised Button, Using a Label</Label></Button>
<Button some-arbitrary-prop="placed on the actual button">Button</Button>

<Fab on:click={() => alert('Clicked!')} extended>
  <Icon class="material-icons" style="margin-right: 12px;">favorite</Icon>
  <Label>Extended FAB</Label>
</Fab>

<Textfield
  bind:value={superText}
  label="Super Text"
  input$aria-controls="super-helper"
  input$aria-describedby="super-helper"
/>
<HelperText id="super-helper">What you put in this box will become super!</HelperText>
```

*Note: The [github repo](https://github.com/hperrin/svelte-material-ui) does not indicate what this should look like; look for rounded corners on the extended fab.*

This example utilises a few components which need to be installed:

```bash
npm install --save-dev @smui/button @smui/fab @smui/textfield @smui/common
```

#### Installation

Lets follow the steps as documented in the [readme](https://github.com/hperrin/svelte-material-ui) (in a different order!)

##### Theme

Start by creating `src/theme/_smui-theme.scss` (for now this can just contain `// Nothing here. Just use the default theme.`).

##### SASS Processor

We have already installed [`rollup-plugin-styles`](https://anidetrix.github.io/rollup-plugin-styles/index.html) which can tools to transpile sass to css; however we need to install those tools (you will get a warning if you run `npm run dev`). Install the module with:

```bash
npm install --save-dev sass
```

Next we need to configure [`rollup-plugin-styles`](https://anidetrix.github.io/rollup-plugin-styles/index.html) to pass include folders to `sass` so that the theme will be found. Open `/rollup.config.js` and replace:

```javascript
styles({
	mode: "extract",
    minimize: production,
}),
```

with:

```javascript
styles({
    mode: "extract",
    minimize: production,
    sass: {
        includePaths: [
            './src/theme',
            './node_modules'
        ]
    }
}),
```

##### Fonts

Fonts were installed in the previous section.

##### Resolve warning when compiling

When running `npm run dev` you will probably get the error `Plugin svelte: A11y: A form label must be associated with a control.`. This is covered in (this issue)[https://github.com/hperrin/svelte-material-ui/issues/164] and the, somewhat hacky, solution given is to silence the error by editing `rollup.config.js` (the below is tweaked to only limit warnings within the `@smui` package) and adding the following to the `svelte` object (after `compilerOptions`):

```javascript
    onwarn: (warning, handler) => {
      // don't warn 'A11y: A form label must be associated with a control'
      if (warning.filename.startsWith('node_modules\\@smui') && warning.code === 'a11y-label-has-associated-control') return;
      // let Rollup handle all other warnings normally
      handler(warning);
    }
```

##### Typography

This is not mentioned in the readme but is in the [sample template](https://github.com/hperrin/smui-example-rollup) so lets add it. Create `/src/App.scss` with the following:

```scss
// Import the typography styles.
//
// MDC Typography
//
@import "@material/typography/mdc-typography";
```

import this in `main.js` by adding `import './App.scss';` at the top.

#### Test

Run `npm run dev` etc to test that this works. Note that some further setup may be required (for example importing `@material/typography/mdc-typography`). The 

## typescript

I plan on using typescript (rather than javascript) so lets convert the template. To do this run:

```bash
node scripts/setupTypeScript.js
npm install
```

Test that everything still works with `npn run dev` (and also `npm run build`).

### Typescript 'sourceMap' compiler option Error

When running a production build you may receive the error:

`(!) Plugin typescript: @rollup/plugin-typescript: Typescript 'sourceMap' compiler option must be set to generate source maps.`

The simplest solution, as per [this issue](https://github.com/sveltejs/template/issues/174), is to edit the `typescript` section in `rollup.config.js` changing `typescript({ sourceMap: !production}),` to `typescript({ sourceMap: true }),`

## tinro - routing

Svelte does not have a standard router. There is one built into [snapper](https://sapper.svelte.dev/) and there are a range of others ()[see svelte-community](https://svelte-community.netlify.app/code?tag=routers)).

As snapper is to be [replaced by SvelteKit](https://svelte.dev/blog/whats-the-deal-with-sveltekit) I did not want to put much effort into learning that. [Routify](https://routify.dev) is very popular but I felt it was over complicated for my needs (it has a cli and application that performs code generation). As I do not need SSR and just wanted something very simple I chose [tinro](https://github.com/AlexxNB/tinro) based upon comments in [Svelte Radio](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3N2ZWx0ZS1yYWRpbw) (29th Nov 2020) (sounded very simple and easy to use).

To install the tinro module run 

```bash
npm install -D tinro
```

To install tinro with `npm install -D tinro`

### Basic Routing

The template really should have some routes if we are to demonstrate tinro. We will move the home page, and a new about page, into separate files (significantly simplifying App.svelte).

The first step is to move the page data out into separate files. I created `Main.svelte` and `About.svelte` (just contains "About sample page"). Next I moved everything between `<main>` and `</main>` in `App.svelte` into `Main.svelte` (along with the supporting javascript from the header). Note that I left `export let name: string;` in both files and tidied up the styles (moving `h1` into `Main.svelte`)

The next step was to setup a very basic route (plain html for now); so `App.svelte` (above styles) is now:

```html
<script lang="ts">
	import {Route} from 'tinro';
	import Main from './Main.svelte';
	import About from './About.svelte';

	export let name: string;
</script>

<main>
	<nav>
		<a href="/">Home</a>
		<a href="/about">About</a>
	</nav>

	<Route path="/"><Main name={name} /></Route>
	<Route path="/about"><About /></Route>
</main>
```

I tested this (`npm run dev`) and it worked as expected 

### Web server settings

The above works fine if you click Home/About. However there is an issue; if we wanted to go straight to the About page the logical thing to do would be to enter `http://127.0.0.1:5000/about` into the address bar but this will not work. The reason for this is that the routing is only on the client side; the web server will return `404 Not Found` for the about page. Fortunately there is a simple solution to this; open `package.json` and add the argument `--single` to all calls to `serv` i.e.

 * `"start": "sirv public",` --> `"start": "sirv public --single",`
 * `"serveprod": "sirv dist",` --> `"start": "sirv public --single",`

Restart the server and the routing should work. `--single` instructs the server to return the `index.html` file for any request where the requested file did not exist.

## Tidy up the interface

While everything works it does not look good so lets tidy things up a bit. This section assumes that everything in the previous sections (other than cache busting) is in place. We will add a toolbar with a menu and a couple of buttons, some custom styling and typography examples. Firstly install the needed components:

```bash
npm install -d @smui/icon-button @smui/top-app-bar @smui/menu @smui/list
```

I will not go through the changes in detail (because they are fairly self explanatory); check out the updated files in the repository:

* `src/theme/_smui-theme.scss` - stolen from the [demo](https://github.com/hperrin/svelte-material-ui/blob/master/site/src/theme/_smui-theme.scss).
* `src/About.svelte` - Add typography demo from [here](https://github.com/hperrin/svelte-material-ui/blob/master/site/src/routes/demo/typography.svelte).
* `src/App.svelte` - Add TopAppBar, menu etc

This seems a reasonable point to stop (the template provides a reasonable starting point for a web app).

### Lazy loading

*Note: This has not been applied to the code in the repo because it has no real benefit at this stage*

To check that the production code generation will work with additional chunks I implemented lazy loading. This was fairly easy; the first step being to add `/src/lazy.svelte` (copied directly from [here](https://github.com/AlexxNB/tinro#lazy-loading-components))

```html
<!-- Lazy.svelte-->
<script>
    export let component;
</script>

{#await component.then ? component : component()}
    Loading component...
{:then Cmp}
    <svelte:component this={Cmp.default} />
{/await}
```

The next step was edit `App.svelte` replacing `import About from './About.svelte';` with `import Lazy from './lazy.svelte';` and 

```html
<Route path="/about">
<About/>
```

with:

```html
Lazy component={()=>import('./about.svelte')}/>
```

Finally in `rollup.config.js` replace `format: 'iife',` with `format: 'es',`.

`npm run build` then `npm run serveprod` enables you to test this (seemed to work fine).